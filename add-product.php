<?php
include_once 'classes/product.class.php';
$InsertData = new Product; 
if(isset($_POST["submit"]))  
{  

    $InsertData->setSku($_POST['sku']);
    $InsertData->setName($_POST['name']);
    $InsertData->setPrice($_POST['price']);
    $InsertData->setSize($_POST['size']);
    $InsertData->setproductType($_POST['productType']);
    $InsertData->setheight($_POST['height']);
    $InsertData->setwidth($_POST['width']);
    $InsertData->setlength($_POST['length']);
    $InsertData->setweight($_POST['weight']);
    if( $InsertData->insertProducts() == true)  
    {  
        header("Location: index");
    }       
}  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Exam</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="css/bootstrap.min.css">
         <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <form id="product_form" action="" method="post">
               <div class="product-form ">
                    <div class="row d-flex">
                        <div class="col-md-8"><h2> Product Add</h2></div>
                        <div class="text-right buttons col-md-4">  
                            <input type="submit" class="btn" name="submit" id="savebutton" value="save" />
                            <a href="index" class="btn btn-info">Cancel</a>
                        </div>
                    </div>
               </div>
               <div class="form-data">
                   <div class="form-group">
                        <div><label for="sku">SKU</label>
                            <input type="text" class="form-control" id="sku" onblur="checkSKU();" name="sku" required validate-msg="Please Provide SKU">
                            <span class="skuError error"></span>
                        </div>
                   </div>
                   <div class="form-group">
                        <div><label for="name">Name</label>
                            <input type="text" id="name" class="form-control" name="name" required validate-msg="Please Provide Product Name">
                        </div>
                   </div>
                   <div class="form-group">
                        <div><label for="price">Price</label>
                            <input type="number" id="price" class="form-control" name="price" required validate-msg="Please Provide Price">
                        </div>
                   </div> 
                   <div class="form-group">
                        <div><label for="price">Type Switcher</label>
                            <select class="form-control" id="productType" onchange="return getthe(this);" required name="productType" validate-msg="Please Provide Switcher">
                                <option value="">Select ProductType</option>
                                <option value="DVD">DVD</option>
                                <option value="Furniture">Furniture</option>
                                <option value="book">Book</option>
                            </select>
                        </div>
                   </div>
                   <input type="hidden" id="selected" value="0" name="">
                   <div class="prodct-type-div" >
                    <div style="display: none" id="DVD" >
                        <div><label for="size">Size (MB)</label>
                            <input  type="number" id="size"  validate-msg="Please Provide size" class="DVD form-control" name="size">
                        </div>
                        <span>*Please provide MD format*</span>
                    </div>
                    <div style="display: none" id="Furniture" >
                        <div><label for="height">Height (CM)</label>
                            <input type="text" id="height" class="form-control Furniture" validate-msg="Please Provide height" name="height">
                        </div>
                        <div><label for="width">Width (CM)</label>
                            <input type="text" id="width"  class=" form-control Furniture" validate-msg="Please Provide width" name="width">
                        </div>
                        <div><label for="length">Length (CM)</label>
                            <input type="text" id="length" class="form-control Furniture" validate-msg="Please Provide length" name="length">
                        </div>
                        <span>*Please provide dimensions HxWxL format*</span>
                    </div>
                    <div style="display: none" id="book" >
                        <div><label for="weight">Weight (KG)</label>
                            <input type="text" id="weight" class="book form-control" validate-msg="Please Provide weight" name="weight"  >
                        </div>
                        <span>*Please provide Weight format*</span>
                    </div>
                </div>
               </div>
           </form>
        </div>
    </body>
</html>


<script type="text/javascript">
function getthe(val){
    var old = $('#selected').val();
    $('#'+old).hide();
    $('.'+old).removeAttr('required');
    $('#'+val.value).show();
    $('#selected').val(val.value);
    $('.'+val.value).attr('required','required');
} 
$("#product_form :input").each(function(){
    var input = $(this);
    var msg   = input.attr('validate-msg');
    input.on('change invalid input', function(){
        input[0].setCustomValidity('');
        if(!(input[0].validity.tooLong || input[0].validity.tooShort)){
            if (! input[0].validity.valid) {
                input[0].setCustomValidity(msg);
            }
        }
    });
});
function checkSKU() {
    var sku  = $('#sku').val();
    $.ajax({
        url: "checkSku.php", 
        type:"POST",
        data:{'sku':sku},
        success: function(result){
            if(result==0){
                $(".skuError").html("Sku already added");savebutton
                $('#savebutton').attr('disabled','disabled');
            } else{
                $(".skuError").html("");
                $('#savebutton').removeAttr('disabled');
            }
        }
    });
}
</script>