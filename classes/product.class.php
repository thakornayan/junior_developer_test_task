<?php   
abstract class Databases{  
  public $con;  
  public $error;  
  public function __construct() {  
    $this->con = mysqli_connect("localhost", "root", "", "test");  
    if(!$this->con) {  
      echo 'Database Connection Error ' . mysqli_connect_error($this->con);  
   }  
  }  
}
class Product extends Databases {
  public $id;
  public $sku;
  public $name;
  public $price;
  public $size;
  public $height;
  public $width;
  public $length;
  public $weight;
  public $productType;
  function setId($id){
      $this->id = $id;
  }

  function getId(){
      return $this->id;
  }

  function setSku($sku){
      $this->sku = $sku;
  }

  function getSku(){
      return $this->sku;
  }

  function setName($name){
      $this->name = $name;
  }

  function getName(){
      return $this->name;
  }

  function setPrice($price){
      $this->price = $price;
  }

  function getPrice(){
      return $this->price;
  }

  function setSize($size){
      $this->size = $size;
  }

  function getSize(){
      return $this->size;
  }
  function setheight($height){
      $this->height = $height;
  }

  function getheight(){
      return $this->height;
  }
  function setwidth($width){
      $this->width = $width;
  }

  function getwidth(){
      return $this->width;
  }
  function setlength($length){
      $this->length = $length;
  }

  function getlength(){
      return $this->length;
  }
  function setweight($weight){
      $this->weight = $weight;
  }
  function getweight(){
      return $this->weight;
  }
  function setproductType($productType){
      $this->productType = $productType;
  }
  function getproductType(){
      return $this->productType;
  }
  public function insertProducts(){  
    if($this->getproductType() ==  'DVD'){
      $ProductDvd = new ProductDvd;
      return $ProductDvd->save($this->getSku(),$this->getName(),$this->getPrice(),$this->getproductType(),$this->getSize());
    }
    else if($this->getproductType() ==  'Furniture'){
      $ProductFurniture = new ProductFurniture;
      return $ProductFurniture->save($this->getSku(),$this->getName(),$this->getPrice(),$this->getproductType(),$this->getheight(),$this->getwidth(),$this->getlength());
    }
    else{
      $ProductBook = new ProductBook;
      return $ProductBook->save($this->getSku(),$this->getName(),$this->getPrice(),$this->getproductType(),$this->getweight());
    }
  }  
  public function getProduct($table_name){  
    $array = array();  
    $query = "SELECT * FROM ".$table_name."";  
    $result = mysqli_query($this->con ,$query);  
    while($row = mysqli_fetch_assoc($result)) {  
        $array[] = $row;  
    }  
    return $array;  
  }  
  public function delete($table_name, $where_condition){  
    $query = "DELETE FROM ".$table_name." WHERE  ".$where_condition."";  
    if(mysqli_query($this->con, $query))  
    {  
         return true;  
    }  
  } 
  public function checkSku($table_name,$where){  
    $array = array();  
    $query = "SELECT * FROM ".$table_name."  WHERE  ".$where."";    
    $result = mysqli_query($this->con, $query);  
    $row = mysqli_fetch_array($result);
    return $row;  
  } 
}
class ProductDvd extends Product {
  public function save($getSku,$getName,$getPrice,$getproductType,$getSize)
  {
    $string =  "INSERT INTO product (sku, name, price, productType,size) VALUES ('". $getSku . "','" . $getName. "','" . $getPrice. "','" . $getproductType. "','" . $getSize. "' )";  
    if(mysqli_query($this->con, $string)){  
        return true;  
    }  
    else{  
        echo mysqli_error($this->con);  
    }  
  }
}
class ProductFurniture extends Product {
  public function save($getSku,$getName,$getPrice,$getproductType,$getheight,$getwidth,$getlength)
  {
    $string =  "INSERT INTO product (sku, name, price, productType,height,width,length) VALUES ('". $getSku . "','" . $getName. "','" . $getPrice. "','" . $getproductType. "','" . $getheight. "','" . $getwidth. "','" . $getlength. "' )";  
    if(mysqli_query($this->con, $string)){  
        return true;  
    }  
    else{  
        echo mysqli_error($this->con);  
    }  
  }
}
class ProductBook extends Product {
  public function save($getSku,$getName,$getPrice,$getproductType,$getweight)
  {
    $string =  "INSERT INTO product (sku, name, price, productType,weight) VALUES ('". $getSku . "','" . $getName. "','" . $getPrice. "','" . $getproductType. "','" . $getweight. "' )";  
    if(mysqli_query($this->con, $string)){  
        return true;  
    }  
    else{  
        echo mysqli_error($this->con);  
    }  
  }
}

?>  