<?php
include_once 'classes/product.class.php';
$database = new Product; 
$post_data = $database->getProduct('product'); 
if(isset($_POST["submit"]) && !empty($_POST['id']))  
{ 

	$id = implode(',', $_POST['id']);
 	if($database->delete('product', 'id IN ('.$id.')'))  
    {  
        header("Location: index");
    }     
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Product List</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    		<style type="text/css">
			
		</style>
    </head>
    <body>
    	<form method="post" action="" name="delete" >
        <div class="container-fluid">
           	<div class="product-form ">
                    <div class="row d-flex">
                        <div class="col-md-8"><h2> Product List</h2></div>
                        <div class="text-right buttons col-md-4">  
                          <button  id="ADD" type="button"  class="btn btn-info" name="ADD" onclick="window.location.href='add-product'">ADD</button>    
                        	<button type="submit" name="submit" class="btn btn-secondary"  id="delete-product-btn">MASS DELETE</button>
                        </div>
                    </div>
               </div>
           	<div class="product-list">
           		
           		<?php foreach ($post_data as $key => $value) {
           			$DVD = ($value['productType']=='DVD') ? 'Size : '.$value['size'].' MB' : "";
           			$Furniture = ($value['productType']=='Furniture') ? 'Dimension : '.$value['height'].' x '.$value['width'].' x '.$value['length'] : "";
           			$book = ($value['productType']=='book') ? 'Weight : '.$value['weight'].' KG' : "";
           			?>
	           		<div class="col-md-3 box mt-6" >
	           			<div class="delete-checkbox checkbox"><input type="checkbox" value="<?php echo $value['id']; ?>" name="id[]"> </div>
	           			<span><?php echo $value['sku'] ?></span><br>
	           			<span><?php echo $value['name'] ?></span><br>
	           			<span><?php echo $value['price'] ?> $</span><br>
	           			<span><?php echo $DVD.$Furniture.$book; ?></span>
	           		</div>
       			<?php } ?>
           	</div>
        </div>
		</form>
    </body>
</html>