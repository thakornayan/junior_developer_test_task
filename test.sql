-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2022 at 10:52 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(8) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(8) NOT NULL,
  `productType` varchar(30) NOT NULL,
  `size` varchar(25) DEFAULT NULL,
  `height` varchar(25) DEFAULT NULL,
  `width` varchar(25) DEFAULT NULL,
  `length` varchar(25) DEFAULT NULL,
  `weight` varchar(25) DEFAULT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `sku`, `name`, `price`, `productType`, `size`, `height`, `width`, `length`, `weight`, `createdDate`) VALUES
(6, 'dsf', 'sdf', 35, 'Furniture', '', '234', '234', '234', '', '2022-03-31 06:02:50'),
(9, '345##', '34543', 34, 'book', '', '', '', '', '34', '2022-03-31 08:34:53'),
(11, '345##12', 'sdf', 324, 'DVD', '5000', '', '', '', '', '2022-03-31 09:41:10'),
(12, '#123456', 'asd', 231, 'Furniture', '', '5', '2', '30', '', '2022-03-31 10:01:52'),
(13, 'ssdf', 'sdfsdf', 324, 'DVD', '43', '', '', '', '', '2022-03-31 10:32:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
